package common

import (
	"log"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"

	"gitlab.com/signald/signald-go/signald"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

const (
	OutputFormatDefault  = "default"
	OutputFormatCSV      = "csv"
	OutputFormatTable    = "table"
	OutputFormatJSON     = "json"
	OutputFormatYAML     = "yaml"
	OutputFormatQR       = "qr"
	OutputFormatMarkdown = "md"
	OutputFormatMan      = "man"
	OutputFormatQuiet    = "quiet"

	AnnotationNoSocketConnection = "no-socket"
)

var (
	Signald *signald.Signald

	OutputFormat string
)

func Must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func StylizeTable(t table.Writer) {
	t.SetStyle(table.StyleLight)
}

func StringToAddress(address string) v1.JsonAddress {
	if strings.HasPrefix(address, "+") {
		return v1.JsonAddress{Number: address}
	}
	return v1.JsonAddress{UUID: address}
}
